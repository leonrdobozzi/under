const btnModal = document.querySelector('#open-modal');
const modal = document.querySelector('.modal-form');
const closeModal = document.querySelector('#close-modal');

function showModal(){
    modal.classList.toggle('opened');
}

btnModal.addEventListener('click', () => {
    showModal();
});

closeModal.addEventListener('click', () => {
    showModal();
});